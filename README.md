# mythictable-website
This is the repository for the public website for Mythic Table. It has been set up to deploy using Netlify.

## Operations
WIP

## Quick Start


```
git clone git@gitlab.com:mythicteam/mythictable-website.git
cd mythictable-website
cd gatsbynetlify
npm install
npm start
```

Then you should be able to browse to http://localhost:8000

## Docker Devlopment

This project has been set up to use [Docker Development via VS Code](https://code.visualstudio.com/docs/remote/containers)

### Getting started

* Read the [docs](https://code.visualstudio.com/docs/remote/containers)
* Launch VS Code
* Hit `F1` pr `[ctrl]+[shift]+p` and type/look for `Remote-Containers: Open Folder in Container`
* When it finishes building and the terminal appears with `/app #` use the `gatsby develop` command
* Browse to http://localhost:8000/

### Without VS Code

* TODO

## Common Issues


### MacOS/OSX

If you run into this when running the `npm install`:

```
npm ERR! No receipt for 'com.apple.pkg.CLTools_Executables' found at '/'.
npm ERR!
npm ERR! No receipt for 'com.apple.pkg.DeveloperToolsCLILeo' found at '/'.
npm ERR!
npm ERR! No receipt for 'com.apple.pkg.DeveloperToolsCLI' found at '/'.
npm ERR!
npm ERR! gyp: No Xcode or CLT version detected!
```

Then you need to install some xcode command line tools like so:

`xcode-select --install`

If that doesn't resolve the issue, you should run the following:

`sudo rm -rf $(xcode-select -print-path) && xcode-select --install`
