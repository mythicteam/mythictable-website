import React from 'react';

import kickstarterFlag from '../assets/images/kickstarter/just-funded.png';
import '../assets/sass/components/_ks-flag.scss';

// import into into layout.js, bottom of render return
export default function() {
  return (
    <div className="kickstarter">
      <a href="https://www.kickstarter.com/projects/mythictable/mythic-table" target="_blank" rel="noopener noreferrer" >
        <img src={kickstarterFlag} alt="Click to find out more about our Kickstarter!" />
      </a>
    </div>
  );
}