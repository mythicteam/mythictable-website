import React from "react";
import { render, screen } from "@testing-library/react";
import { StaticQuery } from "gatsby";
import Layout from "../layout";

beforeEach(() => {
  StaticQuery.mockImplementationOnce(({ render }) =>
    render({
      site: {
        siteMetadata: {
          title: `Default Starter`,
        },
      },
    })
  );
});

describe("Layout", () => {
  it("renders children", () => {
    render(
      <Layout>
        <div data-testid="some-child"></div>
      </Layout>
    );
    screen.getByTestId("some-child");
  });
});
