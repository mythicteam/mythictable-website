---
date: '2020-08-03'
title: 'Action System'
summary: 'Action System - Our most powerful feature'
---

# Action System 

One of the most powerful and ambitious features in Mythic Table is the Action System. The Action System is an automation feature that easily allows a game master or player the ability to quickly and easily announce and resource actions on the part of characters, monsters and environments.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/7jaY3tWmzoY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### Core

The core of the Action System is three parts:

* Object Level Permissions.
* Action History.
* Auto Resolution.

Together these features will allow users to easily trigger actions for their characters, have them automatically applied to characters they may not control, and present the owner with history of these actions to review, revoke or revise.

#### Why is this important?

We have noticed that so much time is spent in online games wrestling with the interface to manage the results of character actions when instead that time should be spent narrating these results. The Action System is designed to make sure that 90% of everything we do just works and then to allow for the other 10% to be easy.


