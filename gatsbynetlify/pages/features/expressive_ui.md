---
date: '2020-08-03'
title: 'Expressive UI'
summary: 'Expressive UI - One Of The Cornerstones of Mythic Table'
---

# Expressive UI

User Experience, Ease of Use, Power, and Flexibility are the cornerstones of Mythic Table’s UI. We talk about Getting out of the way of the Story. And we do that in two ways:

1. Minimize the need to leave the main action area.
1. We show instead of say.

Expressive UI for us is the ability to communicate feedback to the players during gameplay. Feedback is a very important part of game design. From color choice, hit animation, and controller vibration to a visual language for the game rules and an intuitive and satisfying action/resolution loop. This is no different for Mythic Table.

<img src="/gifs/features/damage.gif" alt="Expressive UI Gif">

Our goal is not to make a video game. Instead, we chose to use modern tools and techniques to provide the feedback that we have, as technology users, grown to expect from our software. This means:

* Every action results in an obvious response from the UI.
* The game state is easily recognizable.
* Attention should not need to be pulled away from the main game area.

It is our hope that these approaches will set a new standard for online roleplaying experiences and that with time, tools like this will be used for in-person games as well.
