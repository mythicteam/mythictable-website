---
date: '2020-08-03'
title: 'Actions Menu'
summary: 'Customizable Actions Menu'
---

# Actions Menu

Here at Mythic Table we have drawn inspiration from a lot of different sources. With a history in game development, it’s natural that we would look there for inspiration. The Action Menu is an example of this.

<img src="/gifs/features/actionbar.gif" alt="Actions Menu Gif">

This menu is a highly customizable, nested, and expressive menu system. The action menu is sensitive to your character selection in the same way you might expect a Realtime Strategy game to behave when selecting different units.

* Tons of built-in icons.
* Users can upload their own icons.
* Expressive buttons indicate the following:
    * Enable/Disabled.
    * Counters.
    * Progress bars.
* Cooldown timers.
* Nested menus.
* Easy to use functionality.
* Powerful management tools.
* Tags for generic and reusable actions.


