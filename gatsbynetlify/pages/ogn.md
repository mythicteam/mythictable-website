---
date: "2020-11-29"
title: "Mythic Table and OGN"
summary: "Celebrating our partnership with OGN"
---

<a  href="https://www.d20pfsrd.com/" target="_blank">
    <img src="../images/OGN-logo-square-200px-wide.png" alt="Open Gaming Network Logo">
</a>
# Announcement

Mythic Table is proud to announce that we have entered into a partnership with The Open Gaming
Network (OGN). We are excited to share that we have found a kindred spirit in the operators
of https://opengamingnetwork.com/, https://d20pfsrd.com and https://5esrd.com.

## What does this mean?

OGN and Mythic Table are discussing a number of initiatives. The first is a collaboration for the
creation of an Open Game System Reference Standard. This will be used by publishers, tool developers and reference providers to standardize the format of rules, lore, characters and eventually modules and more.

Future plans involve working with them to integrate content into Mythic Table and eventually work on a content marketplace together.

## Does this change Mythic Table?

**No**. Our goals and our philosophies remain the same. We will continue to remain open.
Our source will remain open as will our books. The Mythic Table platform will continue to be free for all and there will be no advertising. We will not be gating features and we will not be enforcing subscriptions. OGN is excited to work with us because of these principles and they do not wish to change them.

## Does this change OGN?

**No**. OGN remains supported by their advertisement sponsors. They will continue to host their great services that you all have enjoyed over the years. You may see references to Mythic Table on their sites but everything else will remain unchanged.

We are very happy to be making this announcement and invite you to celebrate with us. Share this announcement with your friends and give a warm welcome and thanks to OGN.
