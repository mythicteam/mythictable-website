---
date: '2020-09-30'
title: 'Mythic Table September 2020 Report'
summary: 'Where we are and where we are going'
---

## Our First Playable Release is days away!!!!

We are pleased to announce that after months of hard work, we are mere days away from launching the First Playable. We have one feature remaining before we are feature-complete, and the finish line is within sight. The progress is due to an incredible amount of support from our passionate dev team. Read on to see everything that’s been completed and what remains, and the next steps towards our first playable.

## TL;DR

* A huge collaborative effort from our contributors, especially on the QA side - 72 issues created and 52 closed
* We will have a slow roll out of the First Playable to a small group, which will begin October 1st, with public access opening up later this month
* Only one feature remains - guest user support, which is nearing completion

## Development

All the User stories prioritized for First Playable have been completed except for one, Guest User support, which is nearing completion but didn’t quite make it in this month. During the past month we focused a lot on testing and working on polish issues. We created 72 bugs and polish issues during extensive testing in the past month, which is all possible due to the incredible work of our team of QA testers. These revolve around usability and style issues, and the work that was done to resolve them will help ensure people’s first encounter with the First Playable will be an enjoyable one. 

Style improvements are immediately evident upon logging into Mythic Table. A lot of work has been done to polish and improve the overall usability and appearance. A tutorial campaign has been introduced to help ease the user into playing a game using Mythic Table.

![Campaign Screenshot](images/09/september-tutorial-screenshot.png)

This is how we’re currently tracking for work items prioritized for first playable:
* 1 user story remaining (3 completed)
* 4 polish issues remaining
* 1 bug remaining

### Chart 1: First Playable Feature Burndown

![Burndown](images/09/burndown.png)

## Plans

Now that we are within reach of feature-complete, we are moving on to the next step; getting people to actually try out our First Playable. We are going to be slowly rolling out starting October 1 to members of our Discord to try out the game. We are doing this in order to assess our systems load and ensure everything is working properly as we start to open it up to everyone.

As we do this slow roll out, we are also getting ready for our First Playable announcement. We will need your help for this. When the time comes we need to make some noise and raise a little excitement. You can help by sharing our message and telling people about what makes Mythic Table special. 

These are exciting times and we’re looking forward to sharing what we’ve built.

Thanks so much for your continued support!

James Hatheway and the Entire Mythic Table Team<br/>
Co-founder and Director of Technology<br/>
Mythic Table Foundation<br/>

