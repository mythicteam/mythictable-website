---
date: '2020-08-02'
title: 'Features'
summary: 'Features That Will Be Included In Mythic Table'
---

# Features - Our Ideology

Mythic Table has several unique features that revolve around a single guiding principle. Get out of the way of the story. We want a virtual tabletop that adds to the experience. We want it to be intuitive. We want it to make smart decisions that reduce the amount of time spent updating characters, inventory, and scenes. We want it to be fast. We want to share content between games and between friends. We want our next virtual tabletop to be Mythic.

# Novel Features

#### [Auto Resolution](/features/actionsystem/)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/7jaY3tWmzoY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Foremost in our feature set is our concept of Auto Resolution. This feature will allow player actions to affect objects over which they may not necessarily have control. The principle is very simple. When a player attempts an action that directly affects an authorized object, that action is sent as a message to our servers. If a Game Master is present, the message is sent to him for approval. When enabled, Auto Resolution will automatically approve the action, and the game state will be updated. Of course, we're not the first ones to think of this, d20pro has this feature, but our feature will do so much more:

* Our unique history viewer will allow a Game Master to undo unwanted actions quickly.
* Auto Resolution can be enabled on a player by player basis. This feature allows you to control which players can have their actions automatically resolved or to stop trouble-making players from affecting everyone's experience.
* Auto Resolution works with player to player actions allowing players to affect each other when the Game Master is not online.
* Our unique feedback system notifies the player when an action has been undone.
* Our system has a redo option for times when a player has corrected the action. This redo will allow them to preserve their original results.
* Our system will allow the Game Master to edit the action if something has been forgotten. This feature is useful for situations when a buff was forgotten, or the game has some house rule for which the action has not accounted.

#### Data

Our second biggest feature is our content management system. Most Game Masters will tell you that they spend most of their time preparing for games. This is especially true if you're using a virtual tabletop. We will spend hours looking for the right maps or creating them ourselves. Once we have the images ready, getting them into the virtual tabletop can be the hardest part. This can be frustrating when we're reusing maps, monsters, and NPCs from other games. Preparing for games shouldn't be this hard.

Mythic Table aims to fix this problem with how it treats the underlying data. Computers have been around for a while, and this problem was solved a long time ago. It's called a file system. If we treat your data as we might treat a file system, we'll start to notice a lot of problems go away.

* Data is immediately available between games regardless if you are the creator.
* You can organize it as you see fit.
* You can add, remove, edit, move, copy objects easily.
* You can search your data.
* You can import and export.
* This import and export feature opens up the possibility of sharing.

#### We're not stopping there. We have plans to do all kinds of exciting things you might not see in other tabletops. These include:

* All object data stored in a flexible human-readable format like JSON. This direction allows for third party tools and exporters to work seamlessly with Mythic Table.
* Object versioning provides safe migration of your data from version to version.
* Object templates allow for objects to inherit from common parent objects. This technique will allow for some very interesting, time-saving tricks such as; adding class levels to monsters, creating new NPCs, or experimenting with multiclassing.
* Folder and object level permissions.
* And so much more.

# Standard Virtual Tabletop Features

The following is a brief list of standard features that will be included in Mythic Table.

#### Expressive and Dynamic Playing Area

We envision a platform that holds your attention in the story and on the field of play.  Whenever possible secondary windows and screens will be minimized in favor of keeping the game in the action.
* On character feedback.
* [Actions menus.](/features/actionsmenu/)
* Configurable hotkeys.
* [Character focused actions and messaging.](/features/actionsmenu/)
* Customizable UI.
* [Expressive UI.](/features/expressive_ui/)

#### GM Tools

* Streamlined content and asset management.
* Undo and redo functionality.
* Import/Export/Share content like finished maps, custom monsters even full campaigns.

#### Maps

* Square, hex and no grid.
* Panning, zooming, multiple layers, animation, line of sight, dynamic lighting.
* Multiple map management.
* Composite maps.
* Multi-level maps.

#### Characters/Monsters

* Flexible data model.
* Multiple portraits.
* Permissions and ownership.
* [Expressive Tokens with UI that reflects effects, health, ownership, etc.](/features/expressive_ui/)
* [Action System.](/features/actionsystem/)
* Inventory.
* Modifiers - Fully function buff/debuff system.
* Notes, Bios, etc.

#### Equipment

* Stats - size, weight.
* Conditions.
* [Modifiers.](/features/modifiers/)
* Custom Actions.

#### Handouts

* Text/Image.
* Permissions.

#### Chat

* Audio.
* Video.
* Text.

#### Dice Rolling

* Basics.
* Embedded text rolling.
* Macros.
* Character specific macros and actions.

#### Activity Tracker

* Review and search activity.
* Undo/redo actions.
* Hidden activity via permissions model.

#### Combat

* Initiative Systems.
* Combat actions like Attacks and some spells.
* In frame feedback - All actions and results must be expressed on the game board and not in secondary windows like chat, dice logs or whatever.

#### Game System resources

* Rules.
* Monsters.
* Items.
* Classes.
* Spells.
* Skills.

#### And much much more...

