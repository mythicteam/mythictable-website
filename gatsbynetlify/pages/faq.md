---
date: '2021-04-22'
title: 'Mythic Table FAQ'
summary: 'Frequently Asked Questions'
---

### Q: Is Mythic Table available in my language?
A: Localization is very important to us but has yet to be fully implemented.

### Q: Will there be a way to implement character sheets into Mythic Table?
A: Yes! This is absolutely on our roadmap.  We are in discussions about how to make them flexible enough to be utilized across gaming systems.

### Q: How do you use the dice roller?
A: Type into the chat box. For example: 3d6 or 1d6+1 etc.  There are also many functions that can be utilized.  For example: 1d20cs>18 will roll a d20 and display a critical success on any roll greater than 18.  Please check out the documentation here: <a target="_blank" href="https://skizzerz.net/DiceRoller/DiceRoller">Skizzerz Dice Library</a>

### Q: With the dice roller, are the results truly random?
A: We use Skizzerz' DiceRoller library, which uses a cryptographically secure random-number generator: <a target="_blank" href="https://skizzerz.net/DiceRoller/DiceRoller">Skizzerz Dice Library</a>

### Q: Is it possible to host our own Mythic server?
A: Yes.  Though this is currently not a simple process.  It is definitely functionality that we want to simplify for our users but currently is not as high on our list as some of our playability features.

### Q: Hey, any update on how/when we might be able to lock tokens for players?
A: We're going to be tackling this feature when we handle the first couple of steps of object ownership and permissions. It's a big feature

### Q: You guys are thinking about token import from other web services?
A: We have a lot of plans. Integrating with other services is going to be big.  We are investigating the Universal VTT format but long term we want to have a standardized way of bringing content in from a myriad of sources.

### Q: Will there be an API for other developers to integrate their services into Mythic?
A: Yes. It's going to be super flexible.

### Q: Is Mythic Table done development?
A: Mythic Table is a work in progress. It is the minimum we expect people will need to play a game. We know that we're missing a lot of features and we have big BIG plans.

### Q: Will there be a GM version and a Player version? So the GM has access to maps/scenes/tokens and the players could adjust their view or move their own token?
A: Yes.  Mythic Table will be the same codebase but will have features for enabling GM tools for the host. This will start to be developed once we start getting to permissions and object ownership.

### Q: Will MT have accessibility options?
A: One of our Designers is a huge accessibility advocate. We're going to be putting a lot of effort into that.

### Q: What’s next in development of Mythic Table?
A: It really depends on the success of our Kickstarter.  If we are funded our first goal is focused on sharing content but will require other updates such as GM tools, assett tagging and more.  Please visit our <a target="_blank" href="https://www.kickstarter.com/projects/mythictable/mythic-table">Kickstarter</a> page.

### Q: How do I join the team? 
A: 
Become a <a target="_blank" href="https://www.mythictable.com/welcome/promoter">Promoter</a> or <a target="_blank" href="https://www.mythictable.com/welcome/patron">Patron</a> or <a target="_blank" href="https://www.mythictable.com/welcome/developer">Developer</a> 

### Q: how do I invite a player to my Campaign?
A: On the Campaign list screen, click the Info link at the bottom of your campaign.  Next, on the bottom of the info screen is a section that says Invite Players. Click the link icon there to copy a hyperlink.  Send that URL to any players that you want to join your campaign.  (One note, you currently cannot uninvite or block players once you add them.  We are working on this feature.)