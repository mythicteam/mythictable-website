---
date: '2020-07-30'
title: 'My Mythic Story: Marc Faulise, Part 5'
summary: 'Learn a little about how Mythic Table got started'
---

This is the 5th installment of My Mythic Story.  You can find all the previous parts here:

[My Mythic Stories](/team/mythicstories/index/)

I’ve written a little about myself in [Part 1](/team/mythicstories/marc-1/), how we formed the Mythic Table Team in [Part 2](/team/mythicstories/marc-2/), our first stumbling steps building Mythic Table in [Part 3](/team/mythicstories/marc-3/) and finally about my experience as a community manager in [Part 4](/team/mythicstories/marc-4/).  Now I would like to share the details of how we designed the pitch.

This section will discuss a number of business-sensitive subjects.  I’ve debated with myself how much I should disclose, but I want to share as much as possible. I find this process fascinating and I think others will, too.  And I also want to remove the mysticism around business subjects such as marketing, project planning and pitching.  There is a school of thought that teaches us that everything done behind closed doors is nefarious and that for-profit organizations are out to take as much as they can.  This is not true.  I believe that, if organizations were more transparent about their books, this notion would slowly start to change.  This is my attempt to start that change.

Mythic Table is a huge project.  It’s not a AAA game, but it certainly requires much more effort than a couple of dedicated amateurs can manage working in their garage on evenings and weekends.  Why is that?  It has to do with a number of reasons. Firstly, we have an architecture that requires the ability to meet the needs of a number of current rule sets like D&D 5e and Pathfinder, as well as future rules sets like Pathfinder 2e and D&D 6e.  This includes the client, the server and the datastores.  Secondly, the user experience we’re targeting is going to be extremely tricky to build.  We’ll need to solve a number of problems that no other Virtual Tabletop has.  Finally, we want to build this rapidly.  Not only do we want to get the first playable version out fast, but updates must be weekly.  New feature must be coming every week.  Bugs need to be addressed within hours or days of being reported.  All of this speaks to a very professional team that is 100% dedicated to the project.

To get the project funded, Carina, our producer, and myself made a plan.  We needed market research that would help demonstrate the viability of the project.  We needed to demonstrate a need for the product as well as its unique features.  We needed data that shows how much people will be willing to pay for these unique features.  And we needed to estimate how much time and money would be required to make the product.  This is all very standard, but much of this falls outside our combined areas of comfort.  Carina is very familiar with games and building pitches and I could handle costing the project, but neither of us were familiar with doing market research.

Add to this, we were coming in hot.  Ideally, we’d have built a community of supporters that showed without a doubt that this product was viable but we fell very short on our goals.  We had hoped for 10,000 subscribers and were prepared to build our pitch after attracting 1,000 but we ended up with 432.  Instead of building our pitch around sure a solid community with guaranteed users, we need to change tack and build it around a forecast showcasing our ability to build that community.  In the end, the pitch was stronger for it but added risk.  What if we were not able to attract more users?  What if we lost users to a competitor?  D&D Beyond was rumored to be making their own virtual tabletop and their community is very strong.

So, we built a plan around us building our community while under development.  I used my previous estimates to plan how much I might be able to grow the community naturally.  We forecasted this with our expected revenue from subscriptions and even a marketplace.  The results were not promising.  We couldn’t find a way to get the project to break even.

This was daunting but it was not our first set back.  We’ve been taking a very cautious approach to our planning and we already had a back up in the event that we couldn’t get funding.  So, we did a little research and forecasted based on this backup plan.  We were surprised to learn this new strategy was actually viable.  It really didn’t make any sense to us.  How is this possible that failing to get funded was better?  The answer turned out to be simpler than we had thought.  But what was this plan?  Open Mythic Table.

Opening Mythic Table involves three very specific actions on our part:
Open source the base Mythic Table code
Open the books
Build and maintain the infrastructure and provide it absolutely free of charge

You might be asking yourself at this point, “how could our forecasts show better results with this tactic?  Are we planning on having the community develop the software?”  No, we aren’t.  We actually expect opening the source will still require the same development effort from the team.  It might even require more.  Where does the revenue come from if there is not subscription?  It will come in the form of voluntary support from the community.  Our idea is that we want to put the books in front of all of you and show you what we need to make this project a reality and then use a Kickstarter and optional subscriptions to fund the development.

We had no idea if this would work, so we asked our subscribers.  We received overwhelming responses.  It seemed this was something everyone could agree on.  Moreover, over 50% of our community said they would be willing to support development and infrastructure with a monthly contribution.  Even if we were to only get half these numbers, it was still higher than we could expect by gating features behind a paywall.  Almost 75% of them thought running a Kickstarter would be a good idea and would support us through that campaign.  If you recall from Part 4, our subscribers are the best, so these numbers would come down quite a bit if we survey the general community, but this was still very promising.

So, with this in mind we rebuilt our pitch.  If this plan would work with no funding, it would still make a great business strategy.  We still wanted to work with our investors.  They offered a lot more than capital.  They offered support, advice, expertise and so much more.  Getting them on board could only strengthen us.  We moved forward with the pitch but, instead of focusing on things like market share and earning potential, we focused on community support and good will.  We talked about the benefits of allowing our customers to be owners.  Finally, we illustrated the advantages of removing the walls between us and the community and letting the community help us build Mythic Table for all of us.

This, of course, failed.  In [Part 6](/team/mythicstories/marc-6/) I will explain why and what our next steps are.
