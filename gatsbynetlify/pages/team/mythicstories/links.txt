---
date: '2020-07-10'
title: 'Hotlinks'
summary: 'Hotlinks reference page'
---

These are the places where the files from this folder are hotlinked except the ones linked to the index.md file in this folder. These are referenced here in case one needs to move or rename one of the files in here.

index.md -> /gatsbynetlify/src/components/nav.js

mark-6.md -> /gatsbynetlify/pages/team/mythicstories/mark-5.md
mark-5.md -> /gatsbynetlify/pages/team/mythicstories/mark-4.md
mark-4.md -> /gatsbynetlify/pages/team/mythicstories/mark-3.md
mark-3.md -> /gatsbynetlify/pages/team/mythicstories/mark-2.md
mark-2.md -> /gatsbynetlify/pages/team/mythicstories/mark-1.md
mark-1.md -> /gatsbynetlify/pages/team/mythicstories/mark-2.md
mark-1.md -> /gatsbynetlify/pages/team/mythicstories/mark-5.md
mark-2.md -> /gatsbynetlify/pages/team/mythicstories/mark-5.md
mark-3.md -> /gatsbynetlify/pages/team/mythicstories/mark-5.md
mark-4.md -> /gatsbynetlify/pages/team/mythicstories/mark-5.md
index.md -> /gatsbynetlify/pages/team/mythicstories/mark-4.md
index.md -> /gatsbynetlify/pages/team/mythicstories/mark-5.md
index.md -> /gatsbynetlify/pages/team/mythicstories/mark-6.md

mark-1.md -> /gatsbynetlify/pages/team/mythicstories/index.md
mark-2.md -> /gatsbynetlify/pages/team/mythicstories/index.md
mark-3.md -> /gatsbynetlify/pages/team/mythicstories/index.md
mark-4.md -> /gatsbynetlify/pages/team/mythicstories/index.md
mark-5.md -> /gatsbynetlify/pages/team/mythicstories/index.md
mark-6.md -> /gatsbynetlify/pages/team/mythicstories/index.md
