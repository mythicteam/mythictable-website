---
date: '2020-07-05'
title: 'My Mythic Story: Marc Faulise, Part 2'
summary: 'Learn a little about how Mythic Table got started'
---

This is a continuation of this post: [My Mythic Story: Marc Faulise, Part 1](/team/mythicstories/marc-1/)

I spoke previously about my history with D&D and I ended with the formation of the team that makes Mythic Table.  Now, I would like to talk about how we formed the team and our ideals.

The team was formed very haphazardly as word spread that we were planning this new project.  There was no end to the interest and passion to work on this project, but there was also doubt, fear and hesitation,and rightfully so.  This is a huge undertaking and many teams have attempted this and had various degrees of success.  We were not going to be satisfied with just being moderately successful.  It wouldn’t stand to just make another virtual tabletop application that solves the problems with the existing products.  It would have been irresponsible for us to think that, if we did the exact same thing, we would have different results.  This led us to our pillars.  These would be the fundamentals of our project upon which all of our future decisions must rest.

## Pillar 1 - Amazing User Experience

This was the easy one for us.  We knew we wanted to solve this problem.  It was the basis of how the team formed and it was the major missing part of all other solutions on the market today.  Whether they suffer from poor layout, the burden of needing to express so much in a condensed space or a lack of productivity features, there exists no tool that satisfies our needs.  So, User Experience must be one of our primary focuses.  If we fail here, the project fails.

## Pillar 2 - Performance

Performance has two very important meanings for us.  The first is obvious: our software needs to be fast and responsive all the time.  The rendering speed must be fast and it should not deteriorate beyond the point of usability.  The networking needs to be fast. Requests and responses from the server must be timely and their delay must very impact the play experience. The second meaning of performance is not as apparent.  It has to do with the level of productivity of the team.  We will need to respond extremely fast to user concerns. Bugs will need to be fixed quickly.  Features must come online before those of our competitors.  We must be able to quickly adapt to changes in the industry such as supporting new game systems or integrating with new tools.  We strongly believe the only way to achieve this is be fully adopting the DevOps practices that have made tech giants like Google, NetFlix and Facebook so successful.  We’ve already set up pipelines and processes that will enable us to deliver changes within 5 minutes of completing the tasks and we’re on our way to building feedback into every part of our development process.

## Pillar 3 - Community

When we started this project, it was because a handful of us wanted this tool.  If it were only for us, Mythic Table would never get made.  You, the people who are reading this, are our earliest supporters and are just as important as the team itself.  This project will require the full-time dedication of a small team of professional programmers, artists, designers and more.  This will not be cheap.  We will need the support of as many of the community as possible.  What does all this mean?  It means that we need to do things a little differently from a typical software project.  We need to get the community involved in the same way any other stakeholders would be involved. That means regular updates, enlisting feedback, changing direction with regards to popular opinion and building a product for you. Does this mean more work for all of you?  Yes.  We’ll be asking everyone to play our early releases, contribute to surveys and be vocal.  We’ve been very lucky so far and a lot of you have already been very active and vocal.  I hope you all continue to support us.

For more details on how the project has gone so far and learn the details of our successes and our failures, read [Part 3](/team/mythicstories/marc-3/)
