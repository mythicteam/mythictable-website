---
date: '2020-07-10'
title: 'Our Mission and Values'
summary: 'The Mission of Mythic Table and the Values For Achieving It'
---

# Our Mission

To build great online tabletop gaming tools that bring the world together.

# Our Values

#### People first:

We believe people come first in all aspects of our business, from our developers to our users.

#### Openness:

We believe strongly in being open and honest with the world, and we encourage others to do the same.

#### Excellence:

We strive for excellence in all we do. Good software adds value to our lives, and we want to provide this value to as many people as possible. We work hard to break down barriers of complexity, language, and accessibility.

# Way Forward

Mythic Team, the people behind Mythic Table, aims to build great online tabletop gaming tools. We focus on people and their gaming experience first and foremost. We are 100% open, sharing our code, our plans, and our dreams. We believe that easy to use tools will help bring people together the world over. If you share our values and believe in our mission, please support us, and help us make the best gaming experience we can. Together, we will carve a path to a bright future.

Read these pages to learn you can contribute:

* [Become a Promoter](/welcome/promoter/)
* [Become a Patron](/welcome/patron/)
* [Become a Developer](/welcome/developer/)

If you are still unsure about supporting us, please read this learn more:
* [Why support Mythic Table?](/welcome/why/)
