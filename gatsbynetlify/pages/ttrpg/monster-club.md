---
date: '2020-03-20'
title: 'Monster Club'
summary: 'Guide - A new way to play an old game'
---

Monster Club is a casual format for playing Role-playing games under the following conditions:

* The players’ attendance is sporadic
* The players have little to no experience
* The time to play the game is significantly limited
* There is a larger pool of players than a DM can comfortably handle in a single session
* There may be more than one DM

This probably doesn’t describe most gaming groups. Most gaming groups work hard to overcome these shortcomings. However, this scenario is very common in office or school environments. If you would like to introduce your favorite roleplaying game in a place like this, Monster Club might be right for you.

### The format

At its heart, Monster Club is very simple. Players will have a set amount of time to play a group of premade Monsters through a preset dungeon. When the session starts, each player chooses one of the monsters to control as their character. They have a goal to achieve, like finding and retrieving an object, killing an enemy, or rescuing a captive. The dungeon is reset after each session, and players must run through it all over again until they can complete it.

The dungeon is usually designed so that players cannot complete it in their first session. However, they may unlock things that make subsequent attempts easier. The following are options for unlocking:

* New Playable Monsters
* Class levels for a monster
* Equipment and magic items

New Playable Monsters are available for selection at the start of the next session. These monsters tend to be more powerful. This allows players to get past challenges they may have struggled with. Typical mechanics for unlocking New Playable Monsters includes defeating that monster in combat, helping that monster with a quest or challenge, freeing it from captivity, releasing it from a curse, or anything you might imagine. There can be more than one way of doing this, but if it is complex, there should always be a clue to how it is achieved.

Unlocking Class Levels is a mechanic that will allow players to make their favorite monsters more viable during later sessions as the challenges get harder. It is best to limit these to one or two per dungeon and only to allow them to be unlocked once. A good mechanic to use for this is one the player can immediately recognize. Using an altar to a god is an excellent way of doing this. Altars are commonly used to ask for blessings or to make sacrifices. When this happens, the players can be rewarded with the unlockable. The next time players see an altar, they will immediately understand what it means. The type of god might even give an indication as to which class is available to be unlocked. For example, a God of War and Destruction might unlock a Barbarian class while an altar to Beauty and Song might unlock a Bard class, and so on. As these unlocks are very powerful, it is best to allow the unlock to occur only once. Subsequent attempts may grant a bonus, but it will not unlock the class. When this unlock occurs, it is best to update the given monster before the next session to give yourself time to make the conversion as it might not be trivial.

Equipment Unlocks are much more manageable. Like the monsters which are chosen at the beginning of a session, so too can equipment be chosen. It is recommended that only one piece of equipment is available per player and that you start Monster Club with no equipment. It is also recommended that equipment unlocks be somewhat rare.

Finally, the player may eventually solve the first Dungeon. If this happens, a new Dungeon is unlocked. This Dungeon should be designed to be harder. The higher difficulty will accommodate the power of the new Unlocks available to the players at this point. And so Monster Club can continue for as long as there are new Dungeons to unlock.

### The Devil is in the Details

In practice, this game plays out very differently from most roleplaying games. From the outside, it looks like it will be heavily focused on strategy and combat, but this is not necessarily the case. Players will respond to the environment in which they find themselves. For this reason, it is highly recommended that DMs running these games have a little fun with it and put in some interesting encounters and not just combat. Keep in mind that the players are not playing heroes, and Monsters they encounter along the way might not react typically towards them. Many might ignore them. Others might ask for help, while some might start out hostile.

If the DM roleplays, the players will roleplay. It’s that easy. It doesn’t even matter how much experience they have. They will get sucked into the silly voices, the pleas for help, or just identifiable characters. If this is your goal, this format will work very well for this.

On the other side, the format will present several opportunities. As the players repeatedly go through the same dungeon again and again, they will start to get a feeling for it and anticipate what will happen. They will start to speed through certain areas in an attempt to get to someplace new, and they will intentionally avoid other areas that have little value or that are too challenging for them to overcome. This is by design, and when it happens, DMs are encouraged to roll with it. It will be fun for the players as they feel like they are outsmarting the dungeon. It will also open up an opportunity for a little comedy as NPCs might start reacting a little differently to them as though they might recognize the PC monsters from somewhere if they continue to interact with them again and again. Finally, if your session times are limited, this will be important to allow your player a chance to make it all the way to the end.
Preparing for the Game

As this format is designed for short session times and for new players, it is recommended that you use some tools and techniques to speed things along. I have written some of them here:

#### Print Monster Cards

I like to print a sheet for each monster. One side will have a picture of the monster, and the other will have the important stats the player will need to know. You mustn’t copy the monster’s entry and print it out. Make sure the stats are easily digestible by the players. They should be able to find their defenses and attacks quickly as well as any special abilities. Take some time to put some common calculations in there to make it easy for them to understand.

Bring everything they will need

Be sure to bring enough dice for them as well as extra paper and pencils. If you’re playing over lunch, bring napkins and tell your players to arrive a little early if they are going to bring food.

#### Use a Virtual Tabletop

Of course, this will be recommended, but it is not necessary. It will, however, make everything a lot easier. This is especially true for larger groups and more complex maps. It doesn’t matter which VTT you use. I use Roll20 for my games now. You will find that each VTT has strengths and weaknesses when used in this format. Mythic Table will attempt to overcome these. In the meantime, show some love and support to all those other developers that are making tools for us.

#### Simplify things

One of the easiest things to simplify is initiative. Instead of everyone rolling for individual initiative, roll for the person who will start and go around the table clockwise for each subsequent turn. Reduce ability damage to penalties or HP damage. Gloss over distances, movement rates, and terrain types. Just do what feels right.
Scheduling
You may have more players than you feel comfortable managing in the game. This is not a problem. In this format, players can simply jump in and out between sessions without disrupting the overall game. For this reason, you can let players know that if they are unable to make it into one session that they are welcome to join the next. It is recommended that the responsibility of organizing player attendance be giving to one or more of the players. There will be enough for you, the DM, to manage that anything that is not vital that you handle is delegated to someone else.

#### Don’t sweat the balance

The goal of this game is to have a little fun. There may be some significant balance issues in both directions. Don’t worry about that. Some situations will be very easy, and others will be nearly impossible. This is perfectly fine. In fact, this variance in the challenge is precisely what you should be aiming for when you design a dungeon. As players unlock monsters, their power will increase until what was once unthinkably difficult is now trivial. Just make sure that the core path to the main dungeon objective is moderate. More challenging encounters work best as optional encounters with fun rewards behind them. This will allow players to feel like they can still make progress and have something to come back to later.

Some encounters will end up being very trivial for them after a while. When this happens, try not to go into turn-based combat. Instead, narrate how they can easily overcome the obstacle. This will leave time for the more exciting scenarios later in the dungeon.

#### Finally, tieing it all together

One of the things I like to do is to tie everything together. The mechanism I like to use is pretty simple. All of the PCs are monsters summoned by a wizard to do his bidding. I love this one as it gives a tiny bit of story in the beginning that you can expand on later as the dungeons get more and more complex. Feel free to use any mechanic you can imagine. There are no rules for this. Have some fun with it.
