---
date: '2020-11-11'
title: 'Press Pages'
summary: 'Content for members of the press'
---

The following press kits are available
* [Kickstarter in PDF](/press/kickstarter.pdf)
* [First Playable](/press/fp/)
* [First Playable in PDF](/press/fp.pdf)
* [All Mythic Table Logo](/press/logos.zip)

## Logos

[![logo-black-white.png](../images/logos/logo-black-white.png)](/press/logo-black-white.png)
[![logo-white-black.png](../images/logos/logo-white-black.png)](/press/logo-white-black.png)
[![logo-black.png](../images/logos/logo-black.png)](/press/logo-black.png)
[![logo-white.png](../images/logos/logo-white.png)](/press/logo-white.png)
[![logo-long-black-white.png](../images/logos/logo-long-black-white.png)](/press/logo-long-black-white.png)
[![logo-long-black.png](../images/logos/logo-long-black.png)](/press/logo-long-black.png)
[![logo-long-white-black.png](../images/logos/logo-long-white-black.png)](/press/logo-long-white-black.png)
[![logo-long-white.png](../images/logos/logo-long-white.png)](/press/logo-long-white.png)
[![logo-tall-black.png](../images/logos/logo-tall-black.png)](/press/logo-tall-black.png)
[![logo-tall-white.png](../images/logos/logo-tall-white.png)](/press/logo-tall-white.png)
