---
date: '2020-07-10'
title: 'Become a Promoter'
summary: 'How you can support us with your influence as a Promoter'
---

# We need your help!

Mythic Table is a passion project for us. We don’t have the budget for a large ad campaign. Instead, we rely on word of mouth. Please help us spread the word! Become a promoter and tell your family, friends, collegues and followers about us. Join us on our social media and help us build an incredible community.

This is how:
* Follow us on Twitter and retweet or reply: [https://twitter.com/mythictable](https://twitter.com/mythictable)
* Join us on Facebook and share our posts: [https://www.facebook.com/mythictable/](https://www.facebook.com/mythictable/)
* Engage with us on Reddit: [https://www.reddit.com/r/mythictable/](https://www.reddit.com/r/mythictable/)

If you are still unsure about supporting us, read this to learn more:
* [Why support Mythic Table?](/welcome/why/)
